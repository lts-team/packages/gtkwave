<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2023 Tony Bybell <bybell@rocketmail.com> -->
<component type="desktop">
  <id>io.github.gtkwave.GTKWave</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-2.0-or-later</project_license>
  <name>GTKWave</name>
  <developer_name>Tony Bybell</developer_name>
  <update_contact>bybell@rocketmail.com</update_contact>
  <summary>Electronic waveform viewer for viewing simulation results</summary>
  <description>
    <p>
      GTKWave is a fully featured GTK+ based waveform viewer which reads FST and
      GHW files as well as standard Verilog VCD/EVCD files and allows their viewing.
    </p>
    <p>
      The viewer supports both post-mortem viewing of VCD files and interactive viewing of VCD data.
      Tcl scripting and callback capability allow for remote control by other applications.
    </p>
  </description>

  <url type="homepage">http://gtkwave.sourceforge.net/</url>
  <url type="bugtracker">https://github.com/gtkwave/gtkwave/issues</url>
  <url type="help">https://github.com/gtkwave/gtkwave</url>

  <screenshots>
    <screenshot type="default">
      <image>http://gtkwave.sourceforge.net/gtkwave-appdata.png</image>
    </screenshot>
  </screenshots>

  <content_rating type="oars-1.0" />

  <releases>
    <release version="3.3.118" date="2023-10-20">
      <description>
        <p>
        Changes in 3.3.118:
        </p>
        <ul>
            <li>Update xml2stems to handle newer "loc" vs "fl" xml tags</li>
            <li>Change preg_regex_c_1 decl to use regex_t* as datatype</li>
            <li>Move gtkwave.appdata.xml to io.github.gtkwave.GTKWave.metainfo.xml</li>
        </ul>
      </description>
    </release>

    <release version="3.3.117" date="2023-08-08">
      <description>
        <p>
        Changes in 3.3.117:
        </p>
        <ul>
            <li>Fix stems reader processing code broken in 3.3.114</li>
        </ul>
      </description>
    </release>

    <release version="3.3.116" date="2023-05-27">
      <description>
        <p>
        Changes in 3.3.116:
        </p>
        <ul>
            <li>Fix manpage/odt for vcd2fst command switch documentation for zlibpack</li>
            <li>Changed sprintf to snprintf in fstapi.c</li>
        </ul>
      </description>
    </release>

    <release version="3.3.115" date="2023-03-28">
      <description>
        <p>
        Changes in 3.3.115:
        </p>
        <ul>
            <li>Fix VZT reader with -fstrict-aliasing</li>
            <li>Fix use_multi_state condition in vzt_write.c</li>
            <li>Fix for UNDEF vs strings at start of a vzt file</li>
            <li>Fix sleep() time scaling redefine for mingw</li>
            <li>Use MapViewOfFileEx for mmap on Windows (fstapi)</li>
            <li>Define FST_DO_MISALIGNED_OPS on AArch64 (fstapi)</li>
            <li>Fixed attrbegin short length problem</li>
        </ul>
      </description>
    </release>

    <release version="3.3.114" date="2022-11-23">
      <description>
        <p>
        Changes in 3.3.114:
        </p>
        <ul>
            <li>Buffer overflow fixes in FST reader</li>
        </ul>
      </description>
    </release>

    <release version="3.3.113" date="2022-10-04">
      <description>
        <p>
        Changes in 3.3.113:
        </p>
        <ul>
            <li>Dummy release to keep in sync with gtk3 version</li>
        </ul>
      </description>
    </release>

    <release version="3.3.112" date="2022-10-04">
      <description>
        <p>
        Changes in 3.3.112:
        </p>
        <ul>
            <li>Bugfix-only release, no feature adds</li>
            <li>VCD reader fixes for unnamed Icarus begin blocks</li>
            <li>String data type crash fix in fst.c</li>
        </ul>
      </description>
    </release>

    <release version="3.3.111" date="2021-09-01">
      <description>
        <p>
        Changes in 3.3.111:
        </p>
        <ul>
            <li>Fix in fstapi for read start limit time</li>
            <li>Fix xml2stems when begin blocks are in functions</li>
            <li>Skip over decimal point in timescale in viewer</li>
        </ul>
      </description>
    </release>
   </releases>
</component>
